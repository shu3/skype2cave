//[設定]名前部分の表示形式。"%s"の部分にスカイプ名が入る
var name_style = '[Skype]%s';

//変更はここまで。
//ここから動作部分
/*************************************************************/
var objWshShell = new ActiveXObject("WScript.Shell");
var datpath = objWshShell.SpecialFolders("Appdata") + '\\LimeChat2\\scripts\\Cavetube.dat'
var stream = new ActiveXObject('ADODB.Stream');
var dir = String(WScript.ScriptFullName).replace(WScript.ScriptName,"");
var last_id = 0;
var last_timestamp = new Date();
// var last_timestamp = new Date(Date.parse("2014/07/24 00:00:00"));
var sametime_uids = [];

var skype = null;
skype = new ActiveXObject('Skype4COM.Skype');
WScript.ConnectObject(skype, 'Skype_');

if(!Array.indexOf){
  Array.prototype.indexOf = function(target){
    for(var i = 0; i < this.length; i++){
      if(this[i] === target){
        return i;
      }
    }
    return -1;
  }
}

function puts_limechat(uname, body) {
  var name = name_style.replace("%s", uname)
  stream.type = 2;
  stream.charset = 'utf-8';
  stream.open();
  stream.writeText('NAME='+name+"_EndName\r\nCOMMENT="+body+'_EndComment');
  stream.saveToFile(datpath, 2);
  stream.close();
}

function chat_filter(e) {
  for (;!e.atEnd();e.moveNext()) {
    var chat= e.item();
    var chat_timestamp = new Date(Date.parse(chat.Timestamp));
    if (chat_timestamp >= last_timestamp) {
      if (chat_timestamp > last_timestamp) {
        last_timestamp = chat_timestamp;
        sametime_uids = [];
      }
      if (sametime_uids.indexOf(chat.Guid) == -1) {
        WScript.Echo(
          "メッセージを表示: guid=<" + chat.Guid + ">, body=<" + chat.Body
          + ">, timestamp=<" + chat_timestamp +">, sametime_uids=<"+sametime_uids.toString()+">");
        var user = chat.FromDisplayName;
        var body = chat.Body;
        puts_limechat(user, body);
        sametime_uids.push(chat.Guid);
      }
      return;
    }
  };
}

function poll_chat(){
  chat_filter(new Enumerator(skype.Messages));
}

while(true) {
  WScript.Sleep(200);
  poll_chat();
}