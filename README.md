# Skype2Cave

Skypeのチャット内容をCavetube.jsが読み取れる位置に書き込みます。
最終的にLimeChatにSkypeチャットの内容が流れていきます。

## 実行

1. 64bitOSはSkype2Cave64.bat, 32bitOSはxx32.batをダブルクリックしてください。

停止したい場合はコンソールをCtrl+Cで止めるか、ウィンドウのバツを押して停止です。
起動中は受け取ったチャットを片っ端から表示していくので注意が必要です。